﻿$(document).ready(function () {

    $(".delete-button").click(function (e) {
        e.preventDefault();
        var self = $(this);
        var id = self.attr("id");
        var description = self.prop("value");
        $.ajax({
            url: "/ToDoes/AJAXDelete",
            data: {
                id: id
            },
            type: "POST",
            success: function (result) {
                $("#tableDiv").html(result);
            },
            error: function (data) {
                console.log("Error: requesting URL /ToDoes/AJAXEdit", data);
            }
        });

    });
});