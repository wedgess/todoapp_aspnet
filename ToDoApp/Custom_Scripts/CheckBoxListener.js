﻿$(document).ready(function () {
    $(".filled-in").change(function () {
        var self = $(this);
        var id = self.attr("id");
        var checked = self.prop("checked");
        $.ajax({
            url: "/ToDoes/AJAXEdit",
            data: {
                id: id,
                isDone: checked
            },
            type: "POST",
            success: function (result) {
                $("#tableDiv").html(result);
            },
            error: function () {
                console.log("Error: requesting URL /ToDoes/AJAXEdit");
            }
        });
    })
});