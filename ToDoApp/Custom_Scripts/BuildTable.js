﻿$(document).ready(function () {
    $.ajax({
        url: "/ToDoes/BuildToDoTable",
        success: function (result) {
            $("#tableDiv").html(result);
        },
        error: function () {
            console.log("Error: requesting URL /ToDoes/BuildToDoTable");
        }
    });
});