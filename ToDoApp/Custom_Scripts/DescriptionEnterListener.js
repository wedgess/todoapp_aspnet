﻿$(document).ready(function () {
    var originalDesc = ""

    $(".todo-input-item").focus(function (e) {
        originalDesc = $(this).prop("value");
    });

    $(".todo-input-item").keydown(function (e) {
        var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
        if (key == 13) {
            var self = $(this);
            var id = self.attr("id");
            var description = self.prop("value");
            // don't submit if user tries submit an empty value, refill field with original value
            if (description === "") {
                self.val(originalDesc);
                return;
            }
            $.ajax({
                url: "/ToDoes/AJAXEditDescription",
                data: {
                    id: id,
                    description: description
                },
                type: "POST",
                success: function (result) {
                    $("#tableDiv").html(result);
                },
                error: function (data) {
                    console.log("Error: requesting URL /ToDoes/AJAXEditDescription", data);
                }
            });
        }
    });
});