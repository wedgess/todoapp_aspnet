﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ToDoApp.Models;

namespace ToDoApp.Controllers
{
    public class ToDoesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: ToDoes
        public ActionResult Index()
        {
            return View();
        }

        private ApplicationUser GetCurrentUser()
        {
            // get id of current user
            string currentUserId = User.Identity.GetUserId();
            // use lambda expression to get the user by currentuserId
            return db.Users.FirstOrDefault(user => user.Id == currentUserId);
        }

        private IEnumerable<ToDo> GetMyToDoes()
        {

            IEnumerable<ToDo> myTodoes = db.ToDos.ToList().Where(todo => todo.User == GetCurrentUser());

            int completedTodoCount = 0;
            foreach (ToDo todo in myTodoes)
            {
                if (todo.IsDone)
                {
                    completedTodoCount++;
                }
            }

            // get percentage complete
            double completePercent = Math.Round(100f * ((float)completedTodoCount / (float)myTodoes.Count()));
            
            // if is NaN (can be when user has no items) show 0 instead of NaN
            ViewBag.Percent = Double.IsNaN(completePercent) ? 0 : completePercent;

            // get only the current users list of todo items
            return myTodoes;
        }

        public ActionResult BuildToDoTable()
        {
            // only return todo's for the user
            return PartialView("_ToDoTable", GetMyToDoes());
        }

        // GET: ToDoes/Create
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AJAXCreate([Bind(Include = "Id,Description")] ToDo toDo)
        {
            if (ModelState.IsValid)
            {
                // get id of current user
                string currentUserId = User.Identity.GetUserId();
                // use lambda expression to get the user by currentuserId
                ApplicationUser currentUser = db.Users.FirstOrDefault(user => user.Id == currentUserId);
                // add the user to the ToDo item to create the link with Foreign Key
                toDo.User = currentUser;
                toDo.IsDone = false; // always going to be false when creating a todo item
                ModelState.Clear();
                db.ToDos.Add(toDo);
                db.SaveChanges();
            }

            // after creating we want only the table to update, showing new todo item
            return PartialView("_ToDoTable", GetMyToDoes());
        }

        // POST: ToDoes/Edit/5
        public ActionResult AJAXEdit(int? id, bool isDone)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ToDo toDo = db.ToDos.Find(id);
            if (toDo == null)
            {
                return HttpNotFound();
            } else
            {
                toDo.IsDone = isDone;
                db.Entry(toDo).State = EntityState.Modified;
                db.SaveChanges();
            }
            // after editing we want only the table to update, showing todo item
            return PartialView("_ToDoTable", GetMyToDoes());
        }


        public ActionResult AJAXEditDescription(int? id, string description)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ToDo toDo = db.ToDos.Find(id);
            if (toDo == null)
            {
                return HttpNotFound();
            }
            else
            {
                toDo.Description = description;
                db.Entry(toDo).State = EntityState.Modified;
                db.SaveChanges();
            }
            // after editing we want only the table to update, showing todo item
            return PartialView("_ToDoTable", GetMyToDoes());
        }


        public ActionResult AJAXDelete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ToDo toDo = db.ToDos.Find(id);
            if (toDo == null)
            {
                return HttpNotFound();
            } else
            {
                db.ToDos.Remove(toDo);
                db.SaveChanges();
            }
            return PartialView("_ToDoTable", GetMyToDoes());
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
