﻿using System;
using System.Linq.Expressions;
using System.Web.Mvc;
using System.Web.Mvc.Html;

public static class HelperUI
{
    public static MvcHtmlString CheckBoxSimple<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> ex, object htmlAttributes)
    {
        if (typeof(TProperty) != typeof(bool))
        {
            throw new InvalidOperationException("You can only generate checkboxes with boolean properties on your view model");
        }

        var boolExpression = Expression.Lambda<Func<TModel, bool>>(ex.Body, ex.Parameters);

        var resultBody = Expression.Convert(ex.Body, typeof(Boolean));
        var result = Expression.Lambda<Func<TModel, Boolean>>(resultBody, ex.Parameters);

        string checkBoxWithHidden = htmlHelper.CheckBoxFor(boolExpression, htmlAttributes).ToHtmlString().Trim();
        string pureCheckBox = checkBoxWithHidden.Substring(0, checkBoxWithHidden.IndexOf("<input", 1));
        return new MvcHtmlString(pureCheckBox);
    }
}